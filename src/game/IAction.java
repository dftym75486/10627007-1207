package game;

public interface IAction {
  public void Bname();
  public void Bname(String name);
  public void Weapon();
  public void Weapon(String weapon);
  public void Armor();
  public void Armor(String armor);
  public void doAttack();
  public void doAttack(String weapon);
  public void doDefense();
  public void deDefense(String armor);
}
