package game;

import javax.management.relation.Role;
import java.util.List;

public abstract class Roles implements IAction {
    private String name;
    private int level;
    private int exp;
    private int health;
    private int mana;
    private List<String> equipment;
    private String weapon;
    private String armor;

    public Roles (String name,int level,int exp,int health,int mana){
        this.name = name;
        this.level = level;
        this.exp = exp;
        this.health = health;
        this.mana = mana;
    }

    public Roles(String name){
        this(name,1,0,100,30);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getExp() {
        return exp;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getMana() {
        return mana;
    }

    public void setMana(int mana) {
        this.mana = mana;
    }

    public List<String> getEquipment() {
        return equipment;
    }

    public void setEquipment(List<String> equipment) {
        this.equipment = equipment;
    }

    public String getWeapon() {
        return weapon;
    }

    public void setWeapon(String weapon) {
        this.weapon = weapon;
    }

    public String getArmor() {
        return armor;
    }

    public void setArmor(String armor) {
        this.armor = armor;
    }

    @Override
    public String toString(){
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("狀態欄:\n").append(getName()).append(",等級:").append(getLevel()).append("\n");
        return stringBuffer.toString();
    }

}


